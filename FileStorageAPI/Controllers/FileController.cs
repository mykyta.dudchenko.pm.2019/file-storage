using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Business.Models;
using Business.Interfaces;
using AutoMapper;

namespace FileStorageAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IFileService _fileService;
        private readonly IMapper _mapper;
        public FileController(IFileService fileService, IMapper mapper)
        {
            _fileService = fileService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<FileModel>> GetAll()
        {
            return Ok(_fileService.GetAll());
        }
        [HttpGet]
        public ActionResult<IEnumerable<FileModel>> GetByFilter([FromQuery] DateTime date)
        {
            return Ok(_fileService.FilterByDate(date));
        }
        [HttpGet]
        public ActionResult<IEnumerable<FileModel>> GetByFilter([FromQuery] String ex)
        {
            return Ok(_fileService.FilterByExtension(ex));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<FileModel>>> GetById(int id)
        {
            return  Ok(await _fileService.GetByIdAsync(id));
        }

        //POST: /api/books/
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] FileModel FileModel)
        {
            await _fileService.AddAsync(FileModel);
            return Ok(_fileService.GetAll().Last()); 
        }

        //PUT: /api/books/
        [HttpPut]
        public async Task<ActionResult> Update(FileModel FileModel)
        {
            await _fileService.UpdateAsync(FileModel);
            return Ok();
        }

        //DELETE: /api/books/1
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _fileService.DeleteByIdAsync(id);
            return Ok();
        }
    }
}