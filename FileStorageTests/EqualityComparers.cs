﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Business.Models;
using Data.Entities;

namespace Library.Tests
{
    internal class FileEqualityComparer : IEqualityComparer<File>
    {
        public bool Equals([AllowNull] File x, [AllowNull] File y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.FileId == y.FileId
                && x.Created == y.Created
                && x.Updated == y.Updated
                && x.Name == y.Name
                && x.Path == y.Path
                && x.Extension == y.Extension
                && x.Size == y.Size;
        }

        public int GetHashCode([DisallowNull] File obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class BookModelEqualityComparer : IEqualityComparer<FileModel>
    {
        public bool Equals([AllowNull] FileModel x, [AllowNull] FileModel y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.FileId == y.FileId
                && x.Created == y.Created
                && x.Updated == y.Updated
                && x.Name == y.Name
                && x.Path == y.Path
                && x.Extension == y.Extension
                && x.Size == y.Size;
        }

        public int GetHashCode([DisallowNull] FileModel obj)
        {
            return obj.GetHashCode();
        }
    }
}
