using System.Linq;
using System.Threading.Tasks;
using Data;
using Data.Entities;
using Data.Repositories;
using NUnit.Framework;
using System;

namespace Library.Tests.DataTests
{
    [TestFixture]
    public class FileRepositoryTests
    {
        [Test]
        public void FileRepository_FindAll_ReturnsAllValues()
        {
            using (var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var fileRepository = new FileRepository(context);

                var books = fileRepository.FindAll();

                Assert.AreEqual(4, books.Count());
            }
        }

        [Test]
        public async Task FileRepository_GetById_ReturnsSingleValue()
        {
            using (var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var fileRepository = new FileRepository(context);

                var file = await fileRepository.GetByIdAsync(1);

                Assert.AreEqual(1, file.FileId);
                Assert.AreEqual("JPG", file.Extension);
                Assert.AreEqual(@"c:\", file.Path);
                Assert.AreEqual("Test", file.Name);
                Assert.AreEqual(new DateTime(2011, 10, 11), file.Created);
                Assert.AreEqual(new DateTime(2011, 10, 11), file.Updated);
                Assert.AreEqual(5000, file.Size);
            }
        }

        [Test]
        public async Task FileRepository_AddAsync_AddsValueToDatabase()
        {
            using (var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var fileRepository = new FileRepository(context);
                var file = new File(){FileId = 5};

                await fileRepository.AddAsync(file);
                await context.SaveChangesAsync();
                
                Assert.AreEqual(5, context.Files.Count());
            }
        }

        [Test]
        public async Task FileRepository_DeleteByIdAsync_DeletesEntity()
        {
            using (var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var fileRepository = new FileRepository(context);
                
                await fileRepository.DeleteByIdAsync(1);
                await context.SaveChangesAsync();
                
                Assert.AreEqual(3, context.Files.Count());
            }
        }

        [Test]
        public async Task BookRepository_Update_UpdatesEntity()
        {
            using (var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var fileRepository = new FileRepository(context);

                var file = new File { FileId = 1, Extension = "PNG", Size = 2500, Updated = new DateTime(2011, 10, 11), Created = new DateTime(2011, 10, 11), Name = "Test", Path = @"c:\" };

                fileRepository.Update(file);
                await context.SaveChangesAsync();

                Assert.AreEqual(1, file.FileId);
                Assert.AreEqual("PNG", file.Extension);
                Assert.AreEqual(@"c:\", file.Path);
                Assert.AreEqual("Test", file.Name);
                Assert.AreEqual(new DateTime(2011, 10, 11), file.Created);
                Assert.AreEqual(new DateTime(2011, 10, 11), file.Updated);
                Assert.AreEqual(2500, file.Size);
            }
        }
    }
}