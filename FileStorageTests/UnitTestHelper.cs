using System;
using AutoMapper;
using Business;
using Data;
using Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Library.Tests
{
    internal static class UnitTestHelper
    {
        public static DbContextOptions<ApplicationContext> GetUnitTestDbOptions()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new ApplicationContext(options))
            {
                SeedData(context);
            }
            return options;
        }

        public static void SeedData(ApplicationContext context)
        {
            context.Files.Add(new File{ FileId = 1, Extension = "JPG", Size = 5000, Updated = new DateTime(2011, 10, 11), Created = new DateTime(2011, 10, 11), Name = "Test", Path = @"c:\" });
            context.Files.Add(new File { FileId = 2, Extension = "EXE", Size = 600, Updated = new DateTime(2011, 10, 11), Created = new DateTime(2011, 10, 11), Name = "Prog", Path = @"c:\" });
            context.Files.Add(new File { FileId = 3, Extension = "GIF", Size = 70000, Updated = new DateTime(2011, 10, 11), Created = new DateTime(2011, 10, 11), Name = "Heheh", Path = @"c:\" });
            context.Files.Add(new File { FileId = 4, Extension = "PDF", Size = 50000, Updated = new DateTime(2011, 10, 11), Created = new DateTime(2011, 10, 11), Name = "File", Path = @"c:\" });
            context.SaveChanges();
        }

        public static Mapper CreateMapperProfile()
        {
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return new Mapper(configuration);
        }
    }
}