using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Business.Models;
using Data;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Library.Tests.IntegrationTests
{
    [TestFixture]
    public class FileIntegrationTests
    {
        private HttpClient _client;
        private CustomWebApplicationFactory _factory;
        private const string RequestUri = "api/file/";
        
        [SetUp]
        public void SetUp()
        {
            _factory = new CustomWebApplicationFactory();
            _client = _factory.CreateClient();
        }

        
        [Test]
        public async Task BooksController_GetById_ReturnsBookModel()
        {
            var httpResponse = await _client.GetAsync(RequestUri + 1);
            
            httpResponse.EnsureSuccessStatusCode();
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var file = JsonConvert.DeserializeObject<FileModel>(stringResponse);

            Assert.AreEqual(1, file.FileId);
            Assert.AreEqual("JPG", file.Extension);
            Assert.AreEqual(@"c:\", file.Path);
            Assert.AreEqual("Test", file.Name);
            Assert.AreEqual(new DateTime(2011, 10, 11), file.Created);
            Assert.AreEqual(new DateTime(2011, 10, 11), file.Updated);
            Assert.AreEqual(5000, file.Size);
        }
        
        [Test]
        public async Task BooksController_Add_AddsBookToDatabase()
        {
            var file = new FileModel { Extension = "JPG", Size = 2900, Updated = new DateTime(2012, 10, 11), Created = new DateTime(2011, 10, 11), Name = "IntegrationEx", Path = @"c:\" };
            var content = new StringContent(JsonConvert.SerializeObject(file), Encoding.UTF8, "application/json");
            var httpResponse = await _client.PostAsync(RequestUri, content);

            httpResponse.EnsureSuccessStatusCode();
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var fileInResponse = JsonConvert.DeserializeObject<FileModel>(stringResponse);

            using (var test = _factory.Services.CreateScope())
            {
                var context = test.ServiceProvider.GetService<ApplicationContext>();
                var databaseFile = await context.Files.FindAsync(fileInResponse.FileId);
                Assert.AreEqual(databaseFile.FileId, fileInResponse.FileId);
                Assert.AreEqual(databaseFile.Extension, fileInResponse.Extension);
                Assert.AreEqual(databaseFile.Name, fileInResponse.Name);
                Assert.AreEqual(databaseFile.Path, fileInResponse.Path);
                Assert.AreEqual(databaseFile.Created, fileInResponse.Created);
                Assert.AreEqual(databaseFile.Updated, fileInResponse.Updated);
                Assert.AreEqual(databaseFile.Size, fileInResponse.Size);
                Assert.AreEqual(5, context.Files.Count());
            }
        }

        [Test]
        public async Task BooksController_Update_UpdatesBookInDatabase()
        {
            var file = new FileModel{ FileId = 2, Extension = "DLL", Size = 800, Updated = new DateTime(2011, 10, 11), Created = new DateTime(2011, 10, 11), Name = "Prog", Path = @"c:\" };
            var content = new StringContent(JsonConvert.SerializeObject(file), Encoding.UTF8, "application/json");
            var httpResponse = await _client.PutAsync(RequestUri, content);

            httpResponse.EnsureSuccessStatusCode();
            
            using (var test = _factory.Services.CreateScope())
            {
                var context = test.ServiceProvider.GetService<ApplicationContext>();
                var databaseFile = await context.Files.FindAsync(file.FileId);
                Assert.AreEqual(file.FileId, databaseFile.FileId);
                Assert.AreEqual(file.Extension, databaseFile.Extension);
                Assert.AreEqual(file.Name, databaseFile.Name);
                Assert.AreEqual(file.Path, databaseFile.Path);
                Assert.AreEqual(file.Created, databaseFile.Created);
                Assert.AreEqual(file.Updated, databaseFile.Updated);
                Assert.AreEqual(file.Size, databaseFile.Size);
                Assert.AreEqual(4, context.Files.Count());
            }
        }

        [Test]
        public async Task BooksController_DeleteById_DeletesBookFromDatabase()
        {
            var bookId = 1;
            var httpResponse = await _client.DeleteAsync(RequestUri + bookId);

            httpResponse.EnsureSuccessStatusCode();
            
            using (var test = _factory.Services.CreateScope())
            {
                var context = test.ServiceProvider.GetService<ApplicationContext>();
                
                Assert.AreEqual(3, context.Files.Count());
            }
        }
        
        [TearDown]
        public void TearDown()
        {
            _factory.Dispose();
            _client.Dispose();
        }
    }
}