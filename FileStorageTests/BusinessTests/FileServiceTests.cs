using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using Moq;
using NUnit.Framework;

namespace Library.Tests.BusinessTests
{
    public class FileServiceTests
    {
        [Test]
        public void FileService_GetAll_ReturnsFileModels()
        {
            var expected = GetTestFileModels().ToList();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(m => m.FileRepository.FindAll())
                .Returns(GetTestFileEntities().AsEnumerable);
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var actual = FileService.GetAll().ToList();

            for (int i = 0; i < actual.Count; i++)
            {
                Assert.AreEqual(expected[i].FileId, actual[i].FileId);
                Assert.AreEqual(expected[i].Extension, actual[i].Extension);
                Assert.AreEqual(expected[i].Created, actual[i].Created);
                Assert.AreEqual(expected[i].Name, actual[i].Name);
                Assert.AreEqual(expected[i].Path, actual[i].Path);
                Assert.AreEqual(expected[i].Size, actual[i].Size);
                Assert.AreEqual(expected[i].Updated, actual[i].Updated);
            }
        }
        
        private IEnumerable<FileModel> GetTestFileModels()
        {
            return new List<FileModel>()
            {
                new FileModel {FileId = 1, Extension = "JPG", Size = 5000, Updated = new DateTime(2011,10,11),Created = new DateTime(2011,10,11),Name = "Test",Path = @"c:\"},
                new FileModel {FileId = 2, Extension = "EXE", Size = 600, Updated = new DateTime(2011,10,11),Created = new DateTime(2011,10,11),Name = "Prog" ,Path = @"c:\"},
                new FileModel {FileId = 3, Extension = "GIF", Size = 70000, Updated = new DateTime(2011,10,11),Created = new DateTime(2011,10,11),Name = "Heheh",Path = @"c:\"},
                new FileModel {FileId = 4, Extension = "PDF", Size = 50000, Updated = new DateTime(2011,10,11),Created = new DateTime(2011,10,11),Name = "File",Path = @"c:\"}
            };
        }

        [Test]
        public async Task FileService_GetByIdAsync_ReturnsFileModel()
        {
            var expected = GetTestFileModels().First();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(m => m.FileRepository.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(GetTestFileEntities().First);
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var actual = await FileService.GetByIdAsync(1);
            
            Assert.AreEqual(expected.FileId, actual.FileId);
            Assert.AreEqual(expected.Extension, actual.Extension);
            Assert.AreEqual(expected.Created, actual.Created);
            Assert.AreEqual(expected.Updated, actual.Updated);
            Assert.AreEqual(expected.Size, actual.Size);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Path, actual.Path);
        }

        private List<File> GetTestFileEntities()
        {
            return new List<File>()
            {
                new File {FileId = 1, Extension = "JPG", Size = 5000, Updated = new DateTime(2011,10,11),Created = new DateTime(2011,10,11),Name = "Test",Path = @"c:\"},
                new File {FileId = 2, Extension = "EXE", Size = 600, Updated = new DateTime(2011,10,11),Created = new DateTime(2011,10,11),Name = "Prog" ,Path = @"c:\"},
                new File {FileId = 3, Extension = "GIF", Size = 70000, Updated = new DateTime(2011,10,11),Created = new DateTime(2011,10,11),Name = "Heheh",Path = @"c:\"},
                new File {FileId = 4, Extension = "PDF", Size = 50000, Updated = new DateTime(2011,10,11),Created = new DateTime(2011,10,11),Name = "File",Path = @"c:\"}
            };
        }
        
        [Test]
        public async Task FileService_AddAsync_AddsModel()
        {
            //Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.FileRepository.AddAsync(It.IsAny<File>()));
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var file = new FileModel { FileId = 6, Extension = "html", Size = 800, Updated = new DateTime(2011, 10, 11), Created = new DateTime(2011, 10, 11), Name = "Angular app", Path = @"c:\" };

            //Act
            await FileService.AddAsync(file);
            
            //Assert
            mockUnitOfWork.Verify(x => x.FileRepository.AddAsync(It.Is<File>(b => b.Name == file.Name 
            && b.FileId == file.FileId
            && b.Extension == file.Extension
            && b.Created == file.Created
            && b.Updated == file.Updated
            && b.Size == file.Size
            && b.Path == file.Path)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test] 
        public void FileService_AddAsync_ThrowsFileStorageExceptionWithEmptyName()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.FileRepository.AddAsync(It.IsAny<File>()));
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var file = new FileModel { FileId = 6, Extension = "html", Size = 800, Updated = new DateTime(2011, 10, 11), Created = new DateTime(2011, 10, 11), Name = "" , Path = @"c:\" };

            Assert.ThrowsAsync<FileStorageException>(() => FileService.AddAsync(file));
        }
        
        [Test] 
        public void FileService_AddAsync_ThrowsFileStorageExceptionWithEmptyExtension()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.FileRepository.AddAsync(It.IsAny<File>()));
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var file = new FileModel { FileId = 6, Extension = "", Size = 800, Updated = new DateTime(2011, 10, 11), Created = new DateTime(2011, 10, 11), Name = "Angular app", Path = @"c:\" };

            Assert.ThrowsAsync<FileStorageException>(() => FileService.AddAsync(file));
        }
        
        [Test] 
        public void FileService_AddAsync_ThrowsFileStorageExceptionWithEmptyPath()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.FileRepository.AddAsync(It.IsAny<File>()));
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var file = new FileModel { FileId = 6, Extension = "", Size = 800, Updated = new DateTime(2011, 10, 11), Created = new DateTime(2011, 10, 11), Name = "Angular app", Path = @"" };

            Assert.ThrowsAsync<FileStorageException>(() => FileService.AddAsync(file));
        }

        [Test]
        public void FileService_AddAsync_ThrowsFileStorageExceptionWithInvalidDate()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.FileRepository.AddAsync(It.IsAny<File>()));
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var file = new FileModel { FileId = 6, Extension = "", Size = 800, Updated = new DateTime(2011, 9, 11), Created = new DateTime(2011, 10, 11), Name = "Angular app", Path = @"aaaaaa" };

            Assert.ThrowsAsync<FileStorageException>(() => FileService.AddAsync(file));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(100)]
        public async Task FileService_DeleteByIdAsync_DeletesFile(int fileId)
        {
            //Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.FileRepository.DeleteByIdAsync(It.IsAny<int>()));
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            
            //Act
            await FileService.DeleteByIdAsync(fileId);
            
            //Assert
            mockUnitOfWork.Verify(x => x.FileRepository.DeleteByIdAsync(fileId), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }
        
        [Test]
        public async Task FileService_UpdateAsync_UpdatesFile()
        {
            //Arrange
            var file = new FileModel { FileId = 1, Extension = "JPG", Size = 5000, Updated = new DateTime(2011, 10, 11), Created = new DateTime(2011, 10, 11), Name = "Test", Path = @"c:\" };
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.FileRepository.Update(It.IsAny<File>()));
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            
            //Act
            await FileService.UpdateAsync(file);
            
            //Assert
            mockUnitOfWork.Verify(x => x.FileRepository.Update(It.Is<File>(b => b.Name == file.Name
            && b.FileId == file.FileId
            && b.Extension == file.Extension
            && b.Created == file.Created
            && b.Updated == file.Updated
            && b.Size == file.Size
            && b.Path == file.Path)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }
        
        [Test]
        public void FileService_UpdateAsync_ThrowsFileExceptionWithEmptyExtension()
        {
            //Arrange
            var file = new FileModel { FileId = 6, Extension = "", Size = 800, Updated = new DateTime(2011, 10, 11), Created = new DateTime(2011, 10, 11), Name = "Angular app", Path = @"c:\" };
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.FileRepository.Update(It.IsAny<File>()));
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            
            Assert.ThrowsAsync<FileStorageException>(() => FileService.UpdateAsync(file));
        }
        
        [Test]
        public void FileService_UpdateAsync_ThrowsFileExceptionWithEmptyName()
        {
            //Arrange
            var file =  new FileModel { FileId = 6, Extension = "html", Size = 800, Updated = new DateTime(2011, 10, 11), Created = new DateTime(2011, 10, 11), Name = "" , Path = @"c:\" };
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.FileRepository.Update(It.IsAny<File>()));
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            
            Assert.ThrowsAsync<FileStorageException>(() => FileService.UpdateAsync(file));
        }

        [Test]
        public void FileService_UpdateAsync_ThrowsFileExceptionWithEmptyPath()
        {
            //Arrange
            var file = new FileModel { FileId = 6, Extension = "", Size = 800, Updated = new DateTime(2011, 10, 11), Created = new DateTime(2011, 10, 11), Name = "Angular app", Path = @"" };
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.FileRepository.Update(It.IsAny<File>()));
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            Assert.ThrowsAsync<FileStorageException>(() => FileService.UpdateAsync(file));
        }

        [Test]
        public void FileService_UpdateAsync_ThrowsFileStorageExceptionWithInvalidDate()
        {
            //Arrange
            var file = new FileModel { FileId = 6, Extension = "", Size = 800, Updated = new DateTime(2011, 9, 11), Created = new DateTime(2011, 10, 11), Name = "Angular app", Path = @"aaaaaa" };
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.FileRepository.Update(It.IsAny<File>()));
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            
            Assert.ThrowsAsync<FileStorageException>(() => FileService.UpdateAsync(file));
        }

        [Test]
        public void FileService_GetByDate_ReturnsFilesByDate()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.FileRepository.FindAll()).Returns(GetTestFileEntities().AsEnumerable);
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var filter = new DateTime(2011, 10, 11);
            
            //Act
            var filteredBooks = FileService.FilterByDate(filter).ToList();
            
            Assert.AreEqual(4, filteredBooks.Count);
            foreach (var file in filteredBooks)
            {
                Assert.AreEqual(filter, file.Created);
            }
        }
        
        [Test]
        public void BookService_GetByFilter_ReturnsBooksByYear()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.FileRepository.FindAll()).Returns(GetTestFileEntities().AsEnumerable);
            IFileService FileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var filter = "GIF";
            
            var filteredBooks = FileService.FilterByExtension(filter).ToList();
            
            Assert.AreEqual(1, filteredBooks.Count);
            foreach (var file in filteredBooks)
            {
                Assert.AreEqual(filter, file.Extension);
            }
        }
    }
}