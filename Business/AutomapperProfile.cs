using AutoMapper;
using Data.Entities;
using Business.Models;
using System.Linq;

namespace Business
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<File, FileModel>()
                .ReverseMap();
        }
    }
}