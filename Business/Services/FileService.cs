﻿using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;

namespace Business.Services
{
    public class FileService : IFileService
    {
        IUnitOfWork Db { get; set; }
        readonly IMapper mapper;
        public FileService(IUnitOfWork uow, IMapper mapper)
        {
            Db = uow;
            this.mapper = mapper;
        }
        public async Task AddAsync(FileModel model)
        {
            if (model.Name == "" || model.Extension == "" || model.Path == "" || model.Updated > DateTime.Now || model.Created > DateTime.Now
                || model.Created > model.Updated)
            {
                throw new FileStorageException("Invalid input data");
            }
            var file = mapper.Map<FileModel, File>(model);
            await Db.FileRepository.AddAsync(file);
            await Db.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            var file = await Db.FileRepository.GetByIdAsync(modelId);
            //if (file == null)
            //{
            //    throw new FileStorageException("There is no file with such id");
            //}
            await Db.FileRepository.DeleteByIdAsync(modelId);
            await Db.SaveAsync();
        }

        public IEnumerable<FileModel> FilterByDate(DateTime date)
        {
            var files = Db.FileRepository.FindAll();
            if (date == default)
            {
                return mapper.Map<IEnumerable<File>, IEnumerable<FileModel>>(files);
            }
            var res = files.Where(x => x.Updated == date);
            return mapper.Map<IEnumerable<File>, IEnumerable<FileModel>>(res);
        }

        public IEnumerable<FileModel> FilterByExtension(string ex)
        {
            var files = Db.FileRepository.FindAll();
            if (ex == "")
            {
                return mapper.Map<IEnumerable<File>, IEnumerable<FileModel>>(files);
            }
            var res = files.Where(x => x.Extension == ex);
            return mapper.Map<IEnumerable<File>, IEnumerable<FileModel>>(res);
        }

        public IEnumerable<FileModel> GetAll()
        {
            return mapper.Map<IEnumerable<File>, IEnumerable<FileModel>>(Db.FileRepository
                .FindAll());
        }

        public async Task<FileModel> GetByIdAsync(int id)
        {
            var file = await Db.FileRepository.GetByIdAsync(id);
            return mapper.Map<File, FileModel>(file);
        }

        public async Task UpdateAsync(FileModel model)
        {
            if (model.Name == "" || model.Extension == "" || model.Path == "" || model.Updated > DateTime.Now || model.Created > DateTime.Now
                || model.Created > model.Updated)
            {
                throw new FileStorageException("Invalid input data");
            }
            Db.FileRepository.Update(mapper.Map<FileModel, File>(model));
            await Db.SaveAsync();
        }
    }
}
