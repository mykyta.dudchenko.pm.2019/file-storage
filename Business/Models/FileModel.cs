﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class FileModel
    {
        public int FileId { get; set; }
        public string Path { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public long Size { get; set; }
    }
}
