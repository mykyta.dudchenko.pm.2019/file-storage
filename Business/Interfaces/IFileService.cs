﻿using Data.Entities;
using System.Collections.Generic;
using Business.Models;
using System;

namespace Business.Interfaces
{
    public interface IFileService : ICrud<FileModel>
    {
        IEnumerable<FileModel> FilterByDate(DateTime date);
        IEnumerable<FileModel> FilterByExtension(string ex);
    }
}
