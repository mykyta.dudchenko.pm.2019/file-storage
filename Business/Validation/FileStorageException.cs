﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace Business.Validation
{
    [Serializable]
    public class FileStorageException : Exception
    {
        public int Status { get; set; } = 400;
        public string Property { get; protected set; }
        public FileStorageException(string message, string prop) : base(message)
        {
            Property = prop;
        }

        public FileStorageException()
        {
        }

        protected FileStorageException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public FileStorageException(string message) : base(message)
        {
        }

        public FileStorageException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
