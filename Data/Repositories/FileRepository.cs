﻿using Data.Interfaces;
using Data.Entities;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class FileRepository : IFile, IDisposable
    {
        private bool disposedValue = false;
        private readonly ApplicationContext context;

        public FileRepository(ApplicationContext context)
        {
            this.context = context;
        }
        public async Task AddAsync(File entity)
        {
            await context.Files.AddAsync(entity);
            
        }

        public void Delete(File entity)
        {
            context.Files.Remove(entity);
            context.SaveChanges();
        }

        public async Task DeleteByIdAsync(int id)
        {
            File file = await context.Files.FindAsync(id);
            context.Files.Remove(file);
            context.SaveChanges();
        }

        public IEnumerable<File> FindAll()
        {
            return context.Files.ToList();
        }

        public async Task<File> GetByIdAsync(int id)
        {
            return await context.Files.FindAsync(id);
        }

        public void Update(File entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
        }
        public void Save()
        {
            context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    context.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<File> SortByDate()
        {
            return context.Files.OrderBy(x=>x.Created).AsEnumerable();
        }
    }
}
