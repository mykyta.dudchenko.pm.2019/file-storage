using Data.Interfaces;
using Data.Repositories;
using System.Threading.Tasks;

namespace Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext db;
        private FileRepository fileRepository;
        public UnitOfWork(ApplicationContext context) 
        {
            db = context;
        }
        public IFile FileRepository
        {
            get
            {
                if (fileRepository == null)
                    fileRepository = new FileRepository(db);
                return fileRepository;
            }
        }
        public async Task<int> SaveAsync()
        {
            return await db.SaveChangesAsync();
        }
        
    }
}