﻿using System;

namespace Data.Entities
{
    public class File
    {
        public int FileId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Extension { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public long Size { get; set; }
    }
}
