﻿using Data.Entities;
using System.Collections.Generic;

namespace Data.Interfaces
{
    public interface IFile : IRepository<File>
    {
        IEnumerable<File> SortByDate();
    }
}
