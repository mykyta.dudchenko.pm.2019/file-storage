using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IUnitOfWork
    {
        IFile FileRepository { get; }
        Task<int> SaveAsync();
    }
}